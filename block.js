const SHA256 = require('crypto-js/sha256')
class Block {

    constructor(index, data, previousHash = ''){
        this.index = index;
        this.data = data; 
        this.previousHash = previousHash;
        this.nouce = 0;
        this.hash = this.createdHash();
    }
    createdHash(){
        const originalChain = `${this.index}|${this.data}|${this.date}|${this.nouce}`;
        return SHA256(originalChain).toString(); 
    }

    mine(dif){
        while(!this.hash.startsWith(dif)){
            this.nouce++;
            this.hash = this.createdHash();
        }
    }
}

module.exports = Block;